const user = {
    name: "Your Name",
    address: {
      personal: {
        line1: "101",
        line2: "street Line",
        city: "NY",
        state: "WX",
        coordinates: {
          x: 35.12,
          y: -21.49,
        },
      },
      office: {
        city: "City",
        state: "WX",
        area: {
          landmark: "landmark",
        },
        coordinates: {
          x: 35.12,
          y: -21.49,
        },
      },
    },
    contact: {
      phone: {
        home: "xxx",
        office: "yyy",
      },
      other: {
        fax: "234",
      },
      email: {
        home: "xxx",
        office: "yyy",
      },
    },
  };
  

 function flattenObj(parent,user, res = {}){
    for(let key in user){
        let pName = parent  + '_' + key;
        if(typeof user[key] == 'object'){
            flattenObj( pName, user[key], res);
        } else {
            res[pName] = user[key];
        }
    }
    return res;
}

var obj = flattenObj("user",user)
console.log(obj)